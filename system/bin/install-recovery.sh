#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY:37830656:053605d015297a4dc559e797306f08d06f90efaa; then
  applypatch EMMC:/dev/block/platform/11120000.ufs/by-name/BOOT:33265664:aef57336432efc3584c68a5db2636ac15b8fcc6d EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY 053605d015297a4dc559e797306f08d06f90efaa 37830656 aef57336432efc3584c68a5db2636ac15b8fcc6d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi

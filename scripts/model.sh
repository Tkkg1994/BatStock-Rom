#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel
ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
SALES_CODE=`cat /system/omc/sales_code.dat`
sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
NEW_CSC=`cat /tmp/aroma/csc.prop`

mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system

# Change CSC to right model
if grep -q G955 /tmp/BLmodel; then
	sed -i -- 's/G950/G955/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G950/G955/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G950/G955/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G950/G955/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G950F/SM-G955F/g'
else if grep -q G950 /tmp/BLmodel; then
	sed -i -- 's/G955/G950/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G955/G950/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G955/G950/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G955/G950/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G950F/g'
else
	echo "Not a supported model, keep csc config default!"
fi
fi


# Change build.prop to right model
if grep -q G955 /tmp/BLmodel; then
	if grep -q G955F /tmp/BLmodel; then
		echo "Already a G955F model, nothing to change"
	fi
	if grep -q G955N /tmp/BLmodel; then
		sed -i -- 's/G955F/G955N/g' /system/build.prop
		echo "Changed to G955N"
	fi
else if grep -q G950 /tmp/BLmodel; then
	sed -i -- 's/dream2lte/dreamlte/g' /system/build.prop
	sed -i -- 's/ro.sf.lcd_density=420/ro.sf.lcd_density=480/g' /system/build.prop
	sed -i -- 's/ro.sf.init.lcd_density=560/ro.sf.init.lcd_density=640/g' /system/build.prop
	if grep -q G950F /tmp/BLmodel; then
		sed -i -- 's/G955F/G950F/g' /system/build.prop
		echo "Changed to G950F"
	fi
	if grep -q G950N /tmp/BLmodel; then
		sed -i -- 's/G955F/G950N/g' /system/build.prop
		echo "Changed to G950N"
	fi
else
	echo "Not a supported model, keep build.prop default!"
fi
fi

sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat

exit 10


#!/sbin/sh
# Written by Tkkg1994

mount /dev/block/platform/11120000.ufs/by-name/USERDATA /data

if [ ! -d /data/media/0/BatStock ]; then
	mkdir /data/media/0/BatStock
	chmod 777 /data/media/0/BatStock
fi

cp -rf /tmp/aroma /data/media/0/BatStock

find /data/media/0/BatStock/aroma -type f ! -iname "*.prop" -delete

cp -rf /tmp/aroma/.install.log /data/media/0/BatStock/aroma/install.log

exit 10


#!/sbin/sh
# Written by Tkkg1994

aromabuildprop=/tmp/aroma/buildtweaks.prop
buildprop=/system/build.prop

mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system

if grep -q item.1.24=1 /tmp/aroma/samsung.prop; then
	echo "Enable knox/tima since knox has been enabled"
	sed -i -- 's/ro.config.knox=v00/ro.config.knox=v30/g' $buildprop
fi

if grep -q finger=1 $aromabuildprop; then
	echo "# Fingerprint Tweak" >> $buildprop
	echo "fingerprint.unlock=1" >> $buildprop
fi

if grep -q user=1 $aromabuildprop; then
	echo "# Multiuser Tweaks" >> $buildprop
	echo "fw.max_users=30" >> $buildprop
	echo "fw.show_multiuserui=1" >> $buildprop
	echo "fw.show_hidden_users=1" >> $buildprop
	echo "fw.power_user_switcher=1" >> $buildprop
fi

if grep -q fix=1 $aromabuildprop; then
	echo "# Screen mirror fix" >> $buildprop
	echo "wlan.wfd.hdcp=disable" >> $buildprop
fi

exit 10

